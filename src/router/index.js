import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import(/* webpackChunkName: "home" */ "../views/HomeView.vue"),
  },
  {
    path: "/blogs",
    name: "blogs",
    component: () => import(/* webpackChunkName: "blog" */ "../views/BlogsView.vue"),
  },
  {
    path: "/blog/:id",
    name: "blog",
    component: () => import(/* webpackChunkName: "blog" */ "../views/BlogView.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
